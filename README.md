        Backend Siesca

Instalamos Nodejs

    https://nodejs.org/es/

Desde el terminal en la carpeta del proyecto 
instalamos las dependencias que utilizaremos:

    npm install bcrypy express jsonwebtoken mongoose nodemon
    npm install morgan fs formidable 
    npm i --save lodash

Crea una cuenta en mongoDb y configurala, siguendo los
pasos expresados en la pagina web del siguiente link:

    https://medium.com/@hugo.roca/mongodb-como-configuraci%C3%B3n-de-cuenta-en-mongodb-atlas-959cdc7d9f81

Luego de la configuración de mongoDB copiamos la ruta 
de conexión vamos al archivo, la pegamos en el archivo index.js
y ponemos la contraseña que establecimos:

    const mongoUri = 'mongodb+srv://siesca:<password>@cluster0.hvpxn.mongodb.net/<dbname>?retryWrites=true&w=majority';

Luego de esto corremos el proyecto desde el terminal

    npm run dev








