const express = require('express');
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth');

const Client = mongoose.model('Client');

const router = express.Router();

router.use(requireAuth);

router.get('/clients', async (req, res) => {
  const clients = await Client.find();
  res.send(clients);
});

router.post('/add/clients', async (req, res) => {
  const { nameClient, lastName, adress, idClient } = req.body;

  if (!nameClient || !lastName || !idClient) {
    return res
      .status(422)
      .send({ error: 'debes proporcionar los datos obligatorios' });
  }

  try {
    const client = new Client({ nameClient, lastName, adress, idClient, userId: req.user._id });
    await client.save();
    res.send(client);
  } catch (err) {
    res.status(422).send({ error: err.message });
  }
});

router.get('/clients/view/:id', async (req, res, next) => {
  const { id } = req.params;
  const clients = await Client.find({_id: id});
  res.send(clients);
});

router.post('/clients/edit/:id', async (req, res, next) => {
  const { nameClient, lastName, adress, idClient } = req.body;

  if (!nameClient || !lastName || !idClient) {
    return res
      .status(422)
      .send({ error: 'debes proporcionar los datos obligatorios' });
  }

  try {
    const { id } = req.params;
    await Client.deleteMany({_id: id});
    const client = new Client({ nameClient, lastName, adress, idClient, userId: req.user._id });
    await client.save();
    res.send(client);
  } catch (err) {
  res.status(422).send({ error: err.message });
  }
});

router.get('/clients/delete/:id', async (req, res, next) => {
  let { id } = req.params;
  await Client.deleteMany({_id: id});
  res.redirect('/clients');
});

router.get('/clients/:nameClients', async (req, res, next) => {
  let { nameClients } = req.params;
  const clients = await Client.find({nameClient: nameClients});
  res.send(clients);
});

module.exports = router;