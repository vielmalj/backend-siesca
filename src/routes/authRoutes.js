const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const User = mongoose.model('User');

const router = express.Router();

router.post('/signup', async (req, res) => {
  const { usuario, email, password } = req.body;

  try {
    const user = new User({ usuario, email, password });
    await user.save();

    const token = jwt.sign({ userId: user._id }, 'MY_SECRET_KEY');
    res.send({ token });
  } catch (err) {
    return res.status(422).send(err.message);
  }
});

router.post('/signin', async (req, res) => {
  const { usuario, password } = req.body;

  if (!usuario || !password) {
    return res.status(422).send({ error: 'Debe proporcionar usuario y contraseña' });
  }

  const user = await User.findOne({ usuario });
  if (!usuario) {
    return res.status(422).send({ error: 'usuario inválido' });
  }

  try {
    await user.comparePassword(password);
    const token = jwt.sign({ userId: user._id }, 'MY_SECRET_KEY');
    res.send({ token });
  } catch (err) {
    return res.status(422).send({ error: 'usuario o correo electrónico inválido' });
  }
});

router.get('/users', async (req, res) => {
  const users = await User.find();
  res.send(users);
});

router.get('/users/view/:id', async (req, res, next) => {
  const users = await User.findById(req.params.id);
  res.send(users);
});

router.post('/users/edit/:id', async (req, res, next) => {

  const { usuario, email, password } = req.body;  

  if (!usuario || !password || !email) {
    return res
      .status(422)
      .send({ error: 'debes proporcionar los datos obligatorios' });
  }

  try {
    const { id } = req.params;
    await User.deleteMany({_id: id});
    const user = new User({ usuario, email, password });
    await user.save();
    res.redirect('/users');
  } catch (err) {
  res.status(422).send({ error: err.message });
  }
});

router.get('/users/delete/:id', async (req, res, next) => {
  let { id } = req.params;
  await User.deleteMany({_id: id});
  res.redirect('/users');
});

router.get('/users/:usuarioBuscar', async (req, res, next) => {
  let { usuarioBuscar } = req.params;
  const users = await User.find({usuario: usuarioBuscar});
  res.send(users);
});

module.exports = router;
