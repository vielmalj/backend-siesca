const express = require('express');
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth');
const formidable = require('formidable');
const fs = require('fs');
const _ = require('lodash');

const Product = mongoose.model('Product');

const router = express.Router();

router.use(requireAuth);

router.get('/products', async (req, res) => {
  const products = await Product.find();
  res.send(products);
});

router.post('/add/products', async (req, res) => {
  let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Image could not be uploaded"
      })
    }

    const {nameProduct, stock, description} = fields
    let products = new Product(fields);

    if (files.photo) {
      if (files.photo.size > 1000000) {
        return res.status(400).json({
          error: "Image should be lass than 1MB in size"
        })
      }
      products.photo.data = fs.readFileSync(files.photo.path)
      products.photo.contentType = files.photo.type
    }

    products.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: "Error guardando el archivo"
        })
      }
      res.json(result);
    })

  })
});

router.get('/products/view/:id', async (req, res, next) => {
  const { id } = req.params;
  const products = await Product.find({_id: id});
  res.send(products);
});

router.post('/products/edit/:id', async (req, res, next) => {

    const { id } = req.params;
    await Product.deleteMany({_id: id});
    
    let form = new formidable.IncomingForm()
  form.keepExtensions = true
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Image could not be uploaded"
      })
    }

    const {nameProduct, stock, description} = fields
    let products = new Product(fields);

    if (files.photo) {
      if (files.photo.size > 1000000) {
        return res.status(400).json({
          error: "Image should be lass than 1MB in size"
        })
      }
      products.photo.data = fs.readFileSync(files.photo.path)
      products.photo.contentType = files.photo.type
    }

    products.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: "Error guardando el archivo"
        })
      }
    })

  })
    res.redirect('/products');
  
});

router.get('/products/delete/:id', async (req, res, next) => {
  let { id } = req.params;
  await Product.deleteMany({_id: id});
  res.redirect('/products');
});

router.get('/products/:nameProducts', async (req, res, next) => {
  let { nameProducts } = req.params;
  const products = await Product.find({nameProduct: nameProducts});
  res.send(products);
});

module.exports = router;