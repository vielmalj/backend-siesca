const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  nameProduct:{
    type: String,
    unique: true,
    required: true
  },
  stock: {
    type: Number,
    required: true
  },
  description:{
    type: String,
    required: true
  },
  photo: {
    data: Buffer,
    contentType: String
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Product', productSchema);
