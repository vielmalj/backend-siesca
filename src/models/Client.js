const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
  nameClient: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true
  },
  adress: {
    type: String,
    required: true
  },
  idClient: {
    type: Number,
    required: true,
    unique: true
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Client',  clientSchema);